Ant Design Jeecg Vue
====

JEECG-BOOT 基础版本号： 2.2.1（发布日期：20200721） 基础代码同步至20200713

Overview
----

基于 [Ant Design of Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/) 实现的 Ant Design Pro  Vue 版
Jeecg-boot 的前段UI框架，采用前后端分离方案，提供强大代码生成器的快速开发平台。
前端页面代码和后端功能代码一键生成，不需要写任何代码，保持jeecg一贯的强大！！



#### 前端技术
 
- 基础框架：[ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现
- JavaScript框架：Vue
- Webpack
- node
- yarn
- eslint
- @vue/cli 3.2.1
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现



项目下载和运行
----

- 拉取项目代码
```bash
git clone https://github.com/zhangdaiscott/jeecg-boot.git
cd  jeecg-boot/ant-design-jeecg-vue
```

- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- Lints and fixes files
```
yarn run lint
```



其他说明
----

- 项目使用的 [vue-cli3](https://cli.vuejs.org/guide/), 请更新您的 cli

- 关闭 Eslint (不推荐) 移除 `package.json` 中 `eslintConfig` 整个节点代码

- 修改 Ant Design 配色，在文件 `vue.config.js` 中，其他 less 变量覆盖参考 [ant design](https://ant.design/docs/react/customize-theme-cn) 官方说明
```ecmascript 6
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */

          'primary-color': '#F5222D',
          'link-color': '#F5222D',
          'border-radius-base': '4px',
        },
        javascriptEnabled: true,
      }
    }
  }
```



附属文档
----
- [Ant Design Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn)

- [报表 viser-vue](https://viserjs.github.io/demo.html#/viser/bar/basic-bar)

- [Vue](https://cn.vuejs.org/v2/guide)

- [路由/菜单说明](https://github.com/zhangdaiscott/jeecg-boot/tree/master/ant-design-jeecg-vue/src/router/README.md)

- [ANTD 默认配置项](https://github.com/zhangdaiscott/jeecg-boot/tree/master/ant-design-jeecg-vue/src/defaultSettings.js)

- 其他待补充...


备注
----

> @vue/cli 升级后，eslint 规则更新了。由于影响到全部 .vue 文件，需要逐个验证。既暂时关闭部分原本不验证的规则，后期维护时，在逐步修正这些 rules


Docker 镜像使用
----

 ``` 
# 1.修改前端项目的后台域名
    public/index.html  
    域名改成： http://jeecg-boot-system:8080/jeecg-boot
   
# 2.先进入打包前端项目
  yarn run build

# 3.构建镜像
  docker build -t nginx:jeecgboot .

# 4.启动镜像
  docker run --name jeecg-boot-nginx -p 80:80 -d nginx:jeecgboot

# 5.配置host

    # jeecgboot
    127.0.0.1   jeecg-boot-redis
    127.0.0.1   jeecg-boot-mysql
    127.0.0.1   jeecg-boot-system
  
# 6.访问前台项目
  http://localhost:80

``` 

图表配置页面说明
```
表格使用列使用js增强时，需要按以下模板来定义
1、customRender为方法表格列自定义渲染js增强
2、(text, r, index) text为单元格渲染内容 r为行数据 index为行索引值
{
	customRender: function(text, r, index) {
    if(text<1000){     
    	 return '<span style="background-color:green">'+text+'</span>'
    }else if(test<1500){
    	 return '<span style="background-color:yellow">'+text+'</span>'   
    }else{
    	 return '<span style="background-color:red">'+text+'</span>'   
    }
  }
}

3、返回元素添加点击事件 方法不能用箭头函数 只支持click方法 
{
	customRender: function(text, r, index) {
    function handleClick(text, r, index){
      console.log(this)
    }
    return '<a @click="'+handleClick+'">办理</a>'
  }
}

图表使用js增强时，需要按以下模板来定义
事件类型bar、line、pie、transverseBar、radar、funnel
onClick.bar = function (event) {
	console.log(this)//当前vue对象
  console.log(event)//点击图表参数
}

4、增加是否分组配置
该配置为区分是否需要前台使用antv transform()方法展开数据，目前只支持柱状图、折线图、条形图
该参数配置为否时 需要的参数格式为
[
  { month: 'Jan', city: 'Tokyo', temperature: 7 },
  { month: 'Jan', city: 'London', temperature: 3.9 },
  { month: 'Feb', city: 'Tokyo', temperature: 6.9 },
  { month: 'Feb', city: 'London', temperature: 4.2 },
  { month: 'Mar', city: 'Tokyo', temperature: 9.5 },
  { month: 'Mar', city: 'London', temperature: 5.7 }
]
```

图表组合
----

- 1、默认值设置当前年：${currentYear}
- 2、默认值设置当前日期：${currentDate}

访客模式
----
- 1、新增一个用户名为guest密码为：123456的用户
- 2、给guest用户分配一个角色，给该角色分配访客可以访问的页面

k-form-design
----
- 1、新增自定义组件 用户选择框 用户组件设置当前用户为默认值方式 ${username}返回值为用户username
- 2、新增自定义组件 部门选择框 部门组件设置当前用户为默认值方式 ${departName}、${departCode}返回值为部门编码,${id}返回值为部门id
- 3、下拉、复选框、单选等组件字典设置修改为自定义、字典（对应字典表code）、远端数据(表名：数据库表，值、标签：数据库字段名)
- 4、时间框 设置默认当前时间 ${currentDate} 不支持范围选择
- 5、组件新增标签增强属性 需要lable换行或者自定义添加样式可以使用该属性 例如 "数字<br/>输入框" "数字&nbsp;输入框" 目前动态表格不支持此属性
auto设计表单列表
----
- 1、前台组件process/ProcessList
- 2、查看流程状态由按钮改为双击进度字段
auto按流程办理页面
----
- 1、前台组件process/TaskList
- 2、组任务和个人代办合并在一起了，根据行数据类型来区分是组任务还是个人任务，其他操作逻辑和个人任务页面逻辑一样，高级查询还是未实现

online
----
- 1、online表单popup动态传递参数方式1 目前只支持主表和一对一附表，一对多附表暂不支持
--在扩展参数里设置 参数格式：{"param":[{"key":"id","value":"id"}]}
--value 支持'id' || 'tableName.id'等方式 tableName为一对一附表表名
- 3、新增online表单beforeSubmit、beforeDelete、beforeEdit、beforeDelBatch事件,使用方法如下
  ``` javascript
  //beforeSubmit
  beforeSubmit({row,that}){
    return new Promise((resolve, reject)=>{
      //此处模拟等待时间，可能需要发起请求
      setTimeout(()=>{
        if(row.name == 'test'){
          // 当某个字段不满足要求的时候可以reject 
          reject('不能提交测试数据');
        }else{
          resolve();
        }
      },3000)
    })
  }
  //beforeDelete
  beforeDelete({row,that}){
    return new Promise((resolve, reject) => {
      if(row.name == 'test'){
        reject('不可删除test数据');
      }else{
        resolve();
      }
    })     
  }
  //beforeEdit
  beforeEdit({row,that}){
    return new Promise((resolve, reject) => {
      if(row.name == 'test'){
        reject('不可编辑test数据');
      }else{
        resolve();
      }
    })     
  }
  //beforeDelBatch
  beforeDelBatch({row,that,ids}){
    return new Promise((resolve, reject) => {
      if(row.name == 'test'){
        reject('不可删除test数据');
      }else{
        resolve();
      }
    })     
  }
 ```
 ```

系统级 
----
 ``` javascript
 新增系统全局参数，VUE_APP_SEARCH_TYPE=like || ''
 设置为like时，前台会在请求字符串类型参数前后拼接*,实现模糊查询功能，若不需要，设置为空或不设置即可
 ```
