import { axios } from '@/utils/request'

export function queryBacklogList(parameter) { // 待办日历查询
  return axios({
    url: '/backlog/queryBacklogList',
    method: 'get',
    params: parameter
  })
}

export function saveBacklog(parameter) { // 保存自定义待办日历
  return axios({
    url: '/calendarBacklog/save',
    method: 'post',
    data: parameter
  })
}

export function deleteBacklog(parameter) { // 删除自定义待办日历
  return axios({
    url: '/calendarBacklog/delete',
    method: 'delete',
    params: parameter
  })
}

export function deleteBatchBacklog(parameter) { // 批量删除自定义待办日历
  return axios({
    url: '/calendarBacklog/deleteBatch',
    method: 'delete',
    params: parameter
  })
}

