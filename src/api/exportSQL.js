
import { axios } from '@/utils/request'

const baseUrl = '/system/sql'

function downLoad(data, fileName) {
  var eleLink = document.createElement('a')
  eleLink.download = fileName + '.sql'
  eleLink.style.display = 'none'
  var blob = new Blob([data])
  eleLink.href = URL.createObjectURL(blob)
  document.body.appendChild(eleLink)
  eleLink.click()
  document.body.removeChild(eleLink)
}

export default {
  exportOnlCgformSql(parameter, fileName) {
    axios({
      url: baseUrl + '/exportOnlCgformSql',
      method: 'get',
      params: parameter
    }).then(res => {
      downLoad(res, fileName)
    })
  },
  exportOnlCgreportSql(parameter, fileName) {
    axios({
      url: baseUrl + '/exportOnlCgreportSql',
      method: 'get',
      params: parameter
    }).then(res => {
      downLoad(res, fileName)
    })
  },
  exportDiagramConfigurationSql(parameter, fileName) {
    axios({
      url: baseUrl + '/exportDiagramConfigurationSql',
      method: 'get',
      params: parameter
    }).then(res => {
      downLoad(res, fileName)
    })
  },
  exportDiagramCombinationSql(parameter, fileName) {
    axios({
      url: baseUrl + '/exportDiagramCombinationSql',
      method: 'get',
      params: parameter
    }).then(res => {
      downLoad(res, fileName)
    })
  },
  exportSysDictSql(parameter, fileName) {
    axios({
      url: baseUrl + '/exportSysDictSql',
      method: 'get',
      params: parameter
    }).then(res => {
      downLoad(res, fileName)
    })
  }
}
