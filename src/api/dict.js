import { axios } from '@/utils/request'
export function getFlowFormDictData(parameter) { // 根据参数查字典{type:'table/code',value:'',label:'',code:''}
  return axios({
    url: '/sys/dict/getFlowFormDictData',
    method: 'post',
    data: parameter
  })
}
