import { axios } from '@/utils/request'

export function backTask(parameter) { // 流程退回
  return axios({
    url: '/activiti/task/back',
    method: 'get',
    params: parameter
  })
}

export function completeTask(parameter) { // 流程审批提交
  return axios({
    url: '/activiti/task/completeTask',
    method: 'post',
    data: parameter
  })
}

export function entrustTask(parameter) { // 流程委托
  return axios({
    url: '/activiti/task/entrustTask',
    method: 'get',
    params: parameter
  })
}

export function historicFlow(parameter) { // 根据ID查询流程流转历史
  return axios({
    url: '/activiti/history/historicFlow',
    method: 'get',
    params: parameter
  })
}

export function getFlowImgByInstanceId(parameter) { // 根据流程实例查询流程图
  return axios({
    url: '/activiti/instance/getFlowImgByInstanceId',
    method: 'get',
    params: parameter
  })
}

export function claimTask(parameter) { // 流程拾取
  return axios({
    url: '/activiti/task/claimTask',
    method: 'get',
    params: parameter
  })
}

export function suspendProcess(parameter) { // 挂起流程
  return axios({
    url: '/activiti/task/suspendProcess',
    method: 'get',
    params: parameter
  })
}

export function getHistoryNode(parameter) { // 获取已流转节点
  return axios({
    url: '/activiti/history/getHistoryNode',
    method: 'get',
    params: parameter
  })
}

export function rejectTargetNode(parameter) { // 退回到任意任务节点
  return axios({
    url: '/activiti/task/rejectTargetNode',
    method: 'get',
    params: parameter
  })
}

export function cancellation(parameter) { // 作废流程
  return axios({
    url: '/activiti/task/cancellation',
    method: 'get',
    params: parameter
  })
}

export function revocation(parameter) { // 取回流程
  return axios({
    url: '/activiti/task/revocation',
    method: 'get',
    params: parameter
  })
}

export function getApprovalRecord(parameter) { // online表单查看流程
  return axios({
    url: '/activiti/task/getApprovalRecord',
    method: 'get',
    params: parameter
  })
}

export function urging(parameter) { // 流程催办
  return axios({
    url: '/activiti/task/urging',
    method: 'get',
    params: parameter
  })
}
