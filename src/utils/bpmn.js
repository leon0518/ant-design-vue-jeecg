import Vue from 'vue'
import { ACCESS_TOKEN } from '@/store/mutation-types'
/**
 * 根据流程字段状态判断是否禁用删除
 * @param data
 * @param name
 * @returns {true | false}
 */

export function getCheckboxProps(record) {
  return {
    props: {
      disabled: !(record.bpmn_status === '0' || record.bpmn_status === '3' || record.bpmn_status === '5')
    }
  }
}

/**
 * online表单根据流程字段状态判断是否禁用删除
 * @param data
 * @param name
 * @returns {true | false}
 */

export function getCheckboxOnlProps(record) {
  return {
    props: {
      disabled: !(record.bpm_status === '0' || record.bpm_status === '3' || record.bpm_status === '5')
    }
  }
}

/**
 * online表单根据流程字段状态判断是否可发起和编辑
 * @param data
 * @param name
 * @returns {true | false}
 */
export function isCanStartBpm(record, key) {
  if (!key) {
    key = 'bpm_status'
  }

  if (
    record[key] === '0' ||
    record[key] === '' ||
    record[key] == null ||
    record[key] === '3' ||
    record[key] === '5'
  ) {
    return true
  } else {
    return false
  }
}

/**
 * online表单流程打印
 * @param data
 * @param name
 * @returns {true | false}
 */
export function openPrint(data) {
  const token = Vue.ls.get(ACCESS_TOKEN)
  let url = `${window._CONFIG['domianURL']}/jmreport/view/${data.reportCode}?token=${token}`
  url += `&id=${data.formDataId}&processInstanceId=${data.processInstanceId}`
  window.open(url)
}
