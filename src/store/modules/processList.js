const processList = {
  state: {
    res: {
      'designForm': {
        'id': '529dbfb740f2db9621684600bee83d4e',
        'desformCode': 'demo_onl_des',
        'desformName': '在线设计测试demo',
        'desformIcon': null,
        'desformDesignJson': '{"list":[{"hideTitle":false,"remoteAPI":{"executed":false,"url":"http://api.jeecg.com/mock/36/test/jeecg/getRealName"},"name":"用户名","icon":"icon-input","options":{"remoteFunc":"func_1558681196000_53686","hidden":false,"defaultValue":"","dataType":"string","width":"100%","pattern":"","placeholder":"","required":false},"model":"name","rules":[{"type":"string","message":"${title}格式不正确"}],"type":"input","key":"1558681196000_53686"},{"hideTitle":false,"remoteAPI":{"executed":false,"url":""},"name":"性别","icon":"icon-select","options":{"remoteFunc":"func_1558681236000_49174","filterable":false,"clearable":false,"hidden":false,"defaultValue":"","remoteOptions":[],"multiple":false,"remote":false,"required":false,"showLabel":true,"props":{"label":"label","value":"value"},"dictCode":"sex","width":"","options":[{"label":"男","value":"1"},{"label":"女","value":"2"}],"disabled":false,"placeholder":""},"model":"sex","rules":[],"type":"select","key":"1558681236000_49174"},{"hideTitle":false,"remoteAPI":{"executed":false,"url":""},"name":"年龄","icon":"icon-input","options":{"remoteFunc":"func_1558681269000_81812","hidden":false,"defaultValue":"","dataType":"string","width":"100%","pattern":"","placeholder":"","required":false},"model":"age","rules":[{"type":"string","message":"年龄格式不正确"}],"type":"input","key":"1558681269000_81812"},{"hideTitle":false,"remoteAPI":{"executed":false,"url":""},"name":"生日","icon":"icon-date","options":{"remoteFunc":"func_1558681307000_57984","clearable":true,"hidden":false,"defaultValue":"","editable":true,"format":"yyyy-MM-dd","type":"date","required":false,"readonly":false,"startPlaceholder":"","width":"","disabled":false,"placeholder":"","endPlaceholder":"","timestamp":false},"model":"birthday","rules":[],"type":"date","key":"1558681307000_57984"},{"hideTitle":false,"remoteAPI":{"executed":false,"url":""},"name":"个人简介","icon":"icon-diy-com-textarea","options":{"remoteFunc":"func_1558681323000_61380","hidden":false,"defaultValue":"","width":"100%","pattern":"","disabled":false,"placeholder":"","required":false},"model":"descc","rules":[],"type":"textarea","key":"1558681323000_61380"}],"config":{"expand":{"css":"","js":"","url":{"css":"","js":""}},"size":"mini","designMobileView":false,"labelPosition":"right","onlineForm":"eee","dialogOptions":{"padding":{"top":25,"left":25,"bottom":30,"right":25},"top":60,"width":1000},"allowPrint":false,"labelWidth":100,"allowExternalLink":false,"customRequestURL":[{"url":""}],"transactional":true}}',
        'cgformCode': 'eee',
        'parentId': null,
        'parentCode': null,
        'desformType': 1,
        'izOaShow': null,
        'izMobileView': null,
        'createBy': 'admin',
        'createTime': '2019-05-24 14:59:50',
        'updateBy': 'jeecg',
        'updateTime': '2020-07-26 15:31:12'
      },
      'buttonsAuth': {
        'add': true,
        'customColumn': true,
        'getUrl': true,
        'batchAction': true,
        'import': true,
        'edit': true,
        'detail': true,
        'export': true,
        'delete': true,
        'superQuery': true
      },
      desformDesignJson: {
        'list': [
          {
            'type': 'input',
            'label': '输入框',
            'options': {
              'type': 'text',
              'width': '100%',
              'defaultValue': '',
              'placeholder': '请输入',
              'clearable': false,
              'maxLength': null,
              'hidden': false,
              'disabled': false
            },
            'model': 'input_1595906349920',
            'key': 'input_1595906349920',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'number',
            'label': '数字输入框',
            'options': {
              'width': '100%',
              'defaultValue': 0,
              'min': null,
              'max': null,
              'precision': null,
              'step': 1,
              'hidden': false,
              'disabled': false,
              'placeholder': '请输入'
            },
            'model': 'number_1595906351322',
            'key': 'number_1595906351322',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'textarea',
            'label': '文本框',
            'options': {
              'width': '100%',
              'minRows': 4,
              'maxRows': 6,
              'maxLength': null,
              'defaultValue': '',
              'clearable': false,
              'hidden': false,
              'disabled': false,
              'placeholder': '请输入'
            },
            'model': 'textarea_1595906352294',
            'key': 'textarea_1595906352294',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'select',
            'label': '下拉选择器',
            'options': {
              'width': '100%',
              'multiple': false,
              'disabled': false,
              'clearable': false,
              'hidden': false,
              'placeholder': '请选择',
              'dynamicKey': '',
              'dynamic': false,
              'options': [
                {
                  'value': '1',
                  'label': '下拉框1'
                },
                {
                  'value': '2',
                  'label': '下拉框2'
                }
              ],
              'showSearch': false
            },
            'model': 'select_1595906354075',
            'key': 'select_1595906354075',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'button',
            'label': '按钮',
            'options': {
              'type': 'primary',
              'handle': 'submit',
              'dynamicFun': '',
              'hidden': false,
              'disabled': false
            },
            'key': 'button_1595906374547'
          },
          {
            'type': 'text',
            'label': '文字',
            'options': {
              'textAlign': 'left',
              'hidden': false,
              'showRequiredMark': false
            },
            'key': 'text_1595906377066'
          },
          {
            'type': 'html',
            'label': 'HTML',
            'options': {
              'hidden': false,
              'defaultValue': '<strong>HTML</strong>'
            },
            'key': 'html_1595906378049'
          },
          {
            'type': 'alert',
            'label': '警告提示',
            'options': {
              'type': 'success',
              'description': '',
              'showIcon': false,
              'banner': false,
              'hidden': false,
              'closable': false
            },
            'key': 'alert_1595906376113'
          },
          {
            'type': 'dept',
            'label': '部门组件',
            'options': {
              'multiple': false,
              'disabled': false,
              'width': '100%',
              'min': 0,
              'max': 99,
              'clearable': true,
              'placeholder': '请选择',
              'showSearch': false
            },
            'model': 'dept_1595906381519',
            'key': 'dept_1595906381519',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'user',
            'label': '用户组件',
            'options': {
              'multiple': false,
              'disabled': false,
              'width': '100%',
              'min': 0,
              'max': 99,
              'clearable': true,
              'placeholder': '请选择',
              'showSearch': false
            },
            'model': 'user_1595906380351',
            'key': 'user_1595906380351',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'switch',
            'label': '开关',
            'options': {
              'defaultValue': false,
              'hidden': false,
              'disabled': false
            },
            'model': 'switch_1595906373410',
            'key': 'switch_1595906373410',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'batch',
            'label': '动态表格',
            'list': [],
            'options': {
              'scrollY': 0,
              'disabled': false,
              'hidden': false,
              'showLabel': false,
              'hideSequence': false,
              'width': '100%'
            },
            'model': 'batch_1595906371025',
            'key': 'batch_1595906371025'
          },
          {
            'type': 'editor',
            'label': '富文本',
            'list': [],
            'options': {
              'height': 300,
              'placeholder': '请输入',
              'defaultValue': '',
              'chinesization': true,
              'hidden': false,
              'disabled': false,
              'showLabel': false,
              'width': '100%'
            },
            'model': 'editor_1595906371993',
            'key': 'editor_1595906371993',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'checkbox',
            'label': '多选框',
            'options': {
              'disabled': false,
              'hidden': false,
              'defaultValue': [],
              'dynamicKey': '',
              'dynamic': false,
              'options': [
                {
                  'value': '1',
                  'label': '选项1'
                },
                {
                  'value': '2',
                  'label': '选项2'
                },
                {
                  'value': '3',
                  'label': '选项3'
                }
              ]
            },
            'model': 'checkbox_1595906355026',
            'key': 'checkbox_1595906355026',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'cascader',
            'label': '级联选择器',
            'options': {
              'disabled': false,
              'hidden': false,
              'showSearch': false,
              'placeholder': '请选择',
              'clearable': false,
              'dynamicKey': '',
              'dynamic': true,
              'options': [
                {
                  'value': '1',
                  'label': '选项1',
                  'children': [
                    {
                      'value': '11',
                      'label': '选项11'
                    }
                  ]
                },
                {
                  'value': '2',
                  'label': '选项2',
                  'children': [
                    {
                      'value': '22',
                      'label': '选项22'
                    }
                  ]
                }
              ]
            },
            'model': 'cascader_1595906370058',
            'key': 'cascader_1595906370058',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'radio',
            'label': '单选框',
            'options': {
              'disabled': false,
              'hidden': false,
              'defaultValue': '',
              'dynamicKey': '',
              'dynamic': false,
              'options': [
                {
                  'value': '1',
                  'label': '选项1'
                },
                {
                  'value': '2',
                  'label': '选项2'
                },
                {
                  'value': '3',
                  'label': '选项3'
                }
              ]
            },
            'model': 'radio_1595906356130',
            'key': 'radio_1595906356130',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'date',
            'label': '日期选择框',
            'options': {
              'width': '100%',
              'defaultValue': '',
              'rangeDefaultValue': [],
              'range': false,
              'showTime': false,
              'disabled': false,
              'hidden': false,
              'clearable': false,
              'placeholder': '请选择',
              'rangePlaceholder': [
                '开始时间',
                '结束时间'
              ],
              'format': 'YYYY-MM-DD'
            },
            'model': 'date_1595906357246',
            'key': 'date_1595906357246',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'treeSelect',
            'label': '树选择器',
            'options': {
              'disabled': false,
              'multiple': false,
              'hidden': false,
              'clearable': false,
              'showSearch': false,
              'treeCheckable': false,
              'placeholder': '请选择',
              'dynamicKey': '',
              'dynamic': true,
              'options': [
                {
                  'value': '1',
                  'label': '选项1',
                  'children': [
                    {
                      'value': '11',
                      'label': '选项11'
                    }
                  ]
                },
                {
                  'value': '2',
                  'label': '选项2',
                  'children': [
                    {
                      'value': '22',
                      'label': '选项22'
                    }
                  ]
                }
              ]
            },
            'model': 'treeSelect_1595906368793',
            'key': 'treeSelect_1595906368793',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'time',
            'label': '时间选择框',
            'options': {
              'width': '100%',
              'defaultValue': '',
              'disabled': false,
              'hidden': false,
              'clearable': false,
              'placeholder': '请选择',
              'format': 'HH:mm:ss'
            },
            'model': 'time_1595906358345',
            'key': 'time_1595906358345',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'rate',
            'label': '评分',
            'options': {
              'defaultValue': 0,
              'max': 5,
              'disabled': false,
              'hidden': false,
              'allowHalf': false
            },
            'model': 'rate_1595906359580',
            'key': 'rate_1595906359580',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'uploadImg',
            'label': '上传图片',
            'options': {
              'defaultValue': '[]',
              'multiple': false,
              'hidden': false,
              'disabled': false,
              'width': '100%',
              'data': '{}',
              'limit': 3,
              'placeholder': '上传',
              'fileName': 'image',
              'headers': {},
              'action': 'http://127.0.0.1:9000/sys/common/upload',
              'listType': 'picture-card'
            },
            'model': 'uploadImg_1595906366259',
            'key': 'uploadImg_1595906366259',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'slider',
            'label': '滑动输入条',
            'options': {
              'width': '100%',
              'defaultValue': 0,
              'disabled': false,
              'hidden': false,
              'min': 0,
              'max': 100,
              'step': 1,
              'showInput': false
            },
            'model': 'slider_1595906360948',
            'key': 'slider_1595906360948',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'uploadFile',
            'label': '上传文件',
            'options': {
              'defaultValue': '[]',
              'multiple': false,
              'disabled': false,
              'hidden': false,
              'drag': false,
              'downloadWay': 'a',
              'dynamicFun': '',
              'width': '100%',
              'limit': 3,
              'data': '{}',
              'fileName': 'file',
              'headers': {},
              'action': 'http://cdn.kcz66.com/uploadFile.txt',
              'placeholder': '上传'
            },
            'model': 'uploadFile_1595906364619',
            'key': 'uploadFile_1595906364619',
            'rules': [
              {
                'required': false,
                'message': '必填项'
              }
            ]
          },
          {
            'type': 'table',
            'label': '表格布局',
            'trs': [
              {
                'tds': [
                  {
                    'colspan': 1,
                    'rowspan': 1,
                    'list': []
                  },
                  {
                    'colspan': 1,
                    'rowspan': 1,
                    'list': []
                  }
                ]
              },
              {
                'tds': [
                  {
                    'colspan': 1,
                    'rowspan': 1,
                    'list': []
                  },
                  {
                    'colspan': 1,
                    'rowspan': 1,
                    'list': []
                  }
                ]
              }
            ],
            'options': {
              'width': '100%',
              'bordered': true,
              'bright': false,
              'small': true,
              'customStyle': ''
            },
            'key': 'table_1595906398986'
          },
          {
            'type': 'grid',
            'label': '栅格布局',
            'columns': [
              {
                'span': 12,
                'list': []
              },
              {
                'span': 12,
                'list': []
              }
            ],
            'options': {
              'gutter': 0
            },
            'key': 'grid_1595906396817'
          },
          {
            'type': 'card',
            'label': '卡片布局',
            'list': [],
            'key': 'card_1595906394849'
          },
          {
            'type': 'divider',
            'label': '分割线',
            'options': {
              'orientation': 'left'
            },
            'key': 'divider_1595906392898'
          }
        ],
        'config': {
          'layout': 'horizontal',
          'labelCol': {
            'span': 4
          },
          'wrapperCol': {
            'span': 18
          },
          'hideRequiredMark': false,
          'customStyle': '',
          'isPrint': false
        }
      }
    },
    res2: {
      'list': [
        {
          'type': 'card',
          'label': '卡片布局',
          'list': [
            {
              'type': 'input',
              'label': '输入框',
              'icon': 'icon-write',
              'options': {
                'type': 'text',
                'width': '100%',
                'defaultValue': '',
                'placeholder': '请输入',
                'clearable': false,
                'maxLength': null,
                'hidden': false,
                'disabled': false
              },
              'model': 'input_1595921245676',
              'key': 'input_1595921245676',
              'rules': [
                {
                  'required': false,
                  'message': '必填项'
                }
              ]
            }
          ],
          'key': 'card_1595921243522'
        },
        {
          'type': 'grid',
          'label': '栅格布局',
          'columns': [
            {
              'span': 12,
              'list': [
                {
                  'type': 'number',
                  'label': '数字输入框',
                  'icon': 'icon-number',
                  'options': {
                    'width': '100%',
                    'defaultValue': 0,
                    'min': null,
                    'max': null,
                    'precision': null,
                    'step': 1,
                    'hidden': false,
                    'disabled': false,
                    'placeholder': '请输入'
                  },
                  'model': 'number_1595921252637',
                  'key': 'number_1595921252637',
                  'rules': [
                    {
                      'required': false,
                      'message': '必填项'
                    }
                  ]
                }
              ]
            },
            {
              'span': 12,
              'list': [
                {
                  'type': 'textarea',
                  'label': '文本框',
                  'icon': 'icon-edit',
                  'options': {
                    'width': '100%',
                    'minRows': 4,
                    'maxRows': 6,
                    'maxLength': null,
                    'defaultValue': '',
                    'clearable': false,
                    'hidden': false,
                    'disabled': false,
                    'placeholder': '请输入'
                  },
                  'model': 'textarea_1595921254094',
                  'key': 'textarea_1595921254094',
                  'rules': [
                    {
                      'required': false,
                      'message': '必填项'
                    }
                  ]
                }
              ]
            }
          ],
          'options': {
            'gutter': 0
          },
          'key': 'grid_1595921248427'
        },
        {
          'type': 'table',
          'label': '表格布局',
          'trs': [
            {
              'tds': [
                {
                  'colspan': 1,
                  'rowspan': 1,
                  'list': [
                    {
                      'type': 'checkbox',
                      'label': '多选框',
                      'icon': 'icon-duoxuan1',
                      'options': {
                        'disabled': false,
                        'hidden': false,
                        'defaultValue': [],
                        'dynamicKey': '',
                        'dynamic': false,
                        'options': [
                          {
                            'value': '1',
                            'label': '选项1'
                          },
                          {
                            'value': '2',
                            'label': '选项2'
                          },
                          {
                            'value': '3',
                            'label': '选项3'
                          }
                        ]
                      },
                      'model': 'checkbox_1595921261351',
                      'key': 'checkbox_1595921261351',
                      'rules': [
                        {
                          'required': false,
                          'message': '必填项'
                        }
                      ]
                    }
                  ]
                },
                {
                  'colspan': 1,
                  'rowspan': 1,
                  'list': [
                    {
                      'type': 'radio',
                      'label': '单选框',
                      'icon': 'icon-danxuan-cuxiantiao',
                      'options': {
                        'disabled': false,
                        'hidden': false,
                        'defaultValue': '',
                        'dynamicKey': '',
                        'dynamic': false,
                        'options': [
                          {
                            'value': '1',
                            'label': '选项1'
                          },
                          {
                            'value': '2',
                            'label': '选项2'
                          },
                          {
                            'value': '3',
                            'label': '选项3'
                          }
                        ]
                      },
                      'model': 'radio_1595921266929',
                      'key': 'radio_1595921266929',
                      'rules': [
                        {
                          'required': false,
                          'message': '必填项'
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              'tds': [
                {
                  'colspan': 1,
                  'rowspan': 1,
                  'list': [
                    {
                      'type': 'radio',
                      'label': '单选框',
                      'icon': 'icon-danxuan-cuxiantiao',
                      'options': {
                        'disabled': false,
                        'hidden': false,
                        'defaultValue': '',
                        'dynamicKey': '',
                        'dynamic': false,
                        'options': [
                          {
                            'value': '1',
                            'label': '选项1'
                          },
                          {
                            'value': '2',
                            'label': '选项2'
                          },
                          {
                            'value': '3',
                            'label': '选项3'
                          }
                        ]
                      },
                      'model': 'radio_1595921263011',
                      'key': 'radio_1595921263011',
                      'rules': [
                        {
                          'required': false,
                          'message': '必填项'
                        }
                      ]
                    }
                  ]
                },
                {
                  'colspan': 1,
                  'rowspan': 1,
                  'list': [
                    {
                      'type': 'date',
                      'label': '日期选择框',
                      'icon': 'icon-calendar',
                      'options': {
                        'width': '100%',
                        'defaultValue': '',
                        'rangeDefaultValue': [],
                        'range': false,
                        'showTime': false,
                        'disabled': false,
                        'hidden': false,
                        'clearable': false,
                        'placeholder': '请选择',
                        'rangePlaceholder': [
                          '开始时间',
                          '结束时间'
                        ],
                        'format': 'YYYY-MM-DD'
                      },
                      'model': 'date_1595921270082',
                      'key': 'date_1595921270082',
                      'rules': [
                        {
                          'required': false,
                          'message': '必填项'
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ],
          'options': {
            'width': '100%',
            'bordered': true,
            'bright': false,
            'small': true,
            'customStyle': ''
          },
          'key': 'table_1595921258971'
        }
      ],
      'config': {
        'layout': 'horizontal',
        'labelCol': {
          'span': 4
        },
        'wrapperCol': {
          'span': 18
        },
        'hideRequiredMark': false,
        'customStyle': '',
        'isPrint': false
      }
    }
  },
  mutations: {

  },
  actions: {

  }
}
export default processList

