import { getAction } from '@/api/manage'
const seting = {
  state: {
    appName: '欢迎进入 卓臻 企业级快速开发平台',
    appAbbr: '卓臻',
    appDesc: '卓臻 是中国最具影响力的 企业级 快速开发平台',
    appLogo: 'logo.png',
    appLogoWhite: 'logo-white.png',
    helpPath: '',
    allowRegister: true,
    allowPhoneLogin: true,
    allowThirdLogin: true,
    isSetAppLogo: false,
    imgUrl: '',
    imgWhiteUrl: ''
  },
  mutations: {
    setSeting(state, data) {
      Object.keys(data).forEach((key, value) => {
        if (data[key] !== undefined && data[key] !== null ) { 
          state[key] = data[key]
        }
        if (key === 'appLogo') {
          if (data[key] === undefined || data[key] === null) { 
            state['isSetAppLogo'] = false
          } else {
            state['isSetAppLogo'] = true
          }
        }
      })
      document.title = state['appName']
    },
    setImgUrl(state, data) {
      if (state.isSetAppLogo) {
        state.imgUrl = window._CONFIG['domianURL'] + state.appLogo
        state.imgWhiteUrl =  window._CONFIG['domianURL'] + state.appLogoWhite
      } else {
        state.imgUrl = require('@/assets/' + state.appLogo)
        state.imgWhiteUrl =  require('@/assets/' + state.appLogoWhite)
      }
    }
  },
  actions: {
    getSeting({ commit }) {
      return new Promise((resolve, reject) => {
        getAction('site/siteinfo').then(response => {
          if (response.success) {
            commit('setSeting', response.result)
            commit('setImgUrl')
            resolve(response)
          } else {
            resolve(response)
          }
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}
export default seting
