export const CHART_LIST = [
  { text: '柱状图', value: 'bar' },
  { text: '堆叠柱状图', value: 'barStack' },
  { text: '曲线图', value: 'line' },
  { text: '饼图', value: 'pie' },
  { text: '多层饼图', value: 'PieMultiStorey' },
  { text: '条形图', value: 'transverseBarMuiltid' },
  { text: '雷达图', value: 'radar' },
  { text: '漏斗图', value: 'funnel' },
  { text: '数据表格', value: 'table' },
  { text: '树形表格', value: 'treeTable' }
]
