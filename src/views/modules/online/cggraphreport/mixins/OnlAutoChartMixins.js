import { getAction } from '@/api/manage'
import { CHART_LIST } from '../utils/Utils'
export const OnlAutoChartMixins = {
  data() {
    return {
      url: {
        list: '/diagram/diagramConfiguration/getChartsData'
      },
      chartData: [],
      head: {},
      items: [],
      onlineGraphType: CHART_LIST, // 图表类型字典
      spinning: true,
      cggraphreportData: {},
      aliases: []
    }
  },
  computed: {
    graphTypeDictOptions() { // 获取需要渲染的图表类型
      var graphTypes = this.head.graphType ? this.head.graphType.split(',') : []
      var charts = []
      console.log(this.onlineGraphType)
      console.log(CHART_LIST)
      this.onlineGraphType.forEach(function(item, i) {
        if (graphTypes.indexOf(item.value) >= 0 && item.value !== 'table') {
          charts.push(item)
        }
      })
      return charts
    }
  },
  created() {
    this.init()
  },
  methods: {
    init(queryParam) {
      if (this.propsChartData) {
        this.setChartData(JSON.parse(JSON.stringify(this.propsChartData)))
        this.spinning = false
        this.cggraphreportData = JSON.parse(JSON.stringify(this.propsChartData))
        this.setAliases(this.cggraphreportData)
      } else {
        const url = this.$route.fullPath
        const code = this.code || url.replace('/online/cggraphreport/chart/', '')
        this.spinning = true
        if (queryParam) {
          queryParam = encodeURI(JSON.stringify(queryParam))
        }
        getAction(this.url.list, { code: code, params: queryParam || '' }).then((res) => { // 根据code获取渲染数据
          if (res.success) {
            this.setChartData(res.result)
            this.cggraphreportData = res.result
            this.setAliases(this.cggraphreportData)
          }
          if (res.code === 510) {
            this.$message.warning(res.message)
          }
          this.spinning = false
        })
      }
    },
    setChartData(data) { // 设置基础data
      this.head = data.head
      this.items = data.items
      var sqlData = []
      if (data.head.dataType === 'json' && data.head.cgrSql) {
        sqlData = JSON.parse(data.head.cgrSql.replace(/↵/g, ''))
      } else if ((data.head.dataType === 'sql' || data.head.dataType === 'bean') && data.head.aggregate === true) {
        sqlData = data.data.aggregate
      } else if ((data.head.dataType === 'sql' || data.head.dataType === 'bean') && data.head.aggregate === false) {
        sqlData = data.data.data
      }
      this.chartData = sqlData
    },
    getCol() {
      const graphTypes = this.head.graphType.split(',')
      if (this.head.displayTemplate === 'single') {
        return { xl: 24, lg: 24, md: 24, sm: 24 }
      } else if (this.head.displayTemplate === 'double' && graphTypes.length > 1) {
        return { xl: 12, lg: 12, md: 12, sm: 12 }
      } else if (this.head.displayTemplate === 'double' && graphTypes.length <= 1) {
        return { xl: 24, lg: 24, md: 24, sm: 24 }
      }
      return {}
    },
    getChartType(type) { // 获取是否包含改类型图表
      if (this.head.graphType) {
        const graphTypes = this.head.graphType.split(',')
        return graphTypes.indexOf(type) >= 0
      } else {
        return false
      }
    },
    isOnlyTable() { // 判断是否只有table和treeTable
      if (this.head.graphType) {
        const graphTypes = this.head.graphType.split(',')
        let flag = false
        switch (graphTypes.length) {
          case 1:
            flag = graphTypes.indexOf('table') >= 0 || graphTypes.indexOf('treeTable') >= 0
            break
          case 2:
            flag = graphTypes.indexOf('table') >= 0 && graphTypes.indexOf('treeTable') >= 0
            break
          default:
            flag = false
        }
        return flag
      } else {
        return false
      }
    },
    setAliases(data) {
      if (data.items) {
        const groupFields = data.head.groupField.split(',')
        data.items.forEach(item => {
          if (groupFields.indexOf(item.fieldName) >= 0) {
            this.aliases.push({
              field: item.fieldName,
              alias: item.fieldTxt
            })
          }
        })
      }
    }
  }
}
