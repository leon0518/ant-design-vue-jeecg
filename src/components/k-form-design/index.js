// 单独引入
import { KFormDesign, KFormBuild, setFormDesignConfig } from './packages'
import './styles/form-design.less'
import customConfig from './custom/index.js'
const kdfConfig = {
  uploadFile: '/sys/common/upload', // 上传文件地址
  uploadImage: '/sys/common/upload', // 上传图片地址
  uploadFileName: 'file', // 上传文件name
  uploadImageName: 'file', // 上传图片name
  uploadFileData: { biz: 'temp' }, // 上传文件额外参数
  uploadImageData: { biz: 'temp' } // 上传图片额外参数
}
const kdfConfig_ = Object.assign(customConfig, kdfConfig)
setFormDesignConfig(kdfConfig_)
export {
  KFormDesign,
  KFormBuild
}

// 全局注册
// k-form-design组件
// import KFormDesign from './components/k-form-design/packages'
// import './components/k-form-design/styles/form-design.less'
// import customConfig from './components/k-form-design/custom/index.js'
// const kdfConfig = {
//   uploadFile: '/sys/common/upload', // 上传文件地址
//   uploadImage: '/sys/common/upload', // 上传图片地址
//   uploadFileName: 'file', // 上传文件name
//   uploadImageName: 'file', // 上传图片name
//   uploadFileData: { biz: 'temp' }, // 上传文件额外参数
//   uploadImageData: { biz: 'temp' } // 上传图片额外参数
// }
// const kdfConfig_ = Object.assign(customConfig, kdfConfig)
// KFormDesign.setConfig(kdfConfig_)
// Vue.use(KFormDesign)
