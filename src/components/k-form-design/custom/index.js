// 引入自定义组件
// import CustomComponent from './CustomComponent/test'
import User from './CustomComponent/user'
import Dept from './CustomComponent/dept'
const customConfig =
{
  title: '自定义字段',
  list: [
    {
      type: 'user', // 组件类型
      label: '用户组件', // 组件名称
      labelHTML: '', // 标题增强
      icon: 'icon-zidingyiyemian',
      component: User, // 组件
      options: {
        defaultValue: '', // 默认值
        multiple: false, // 多选
        disabled: false, // 禁用
        hidden: false, // 是否隐藏，false显示，true隐藏
        width: '100%', // 宽度
        min: 0, // 最小值
        max: 99, // 最大值
        clearable: true, // 可清除
        placeholder: '请选择', // 占位内容
        showSearch: false, // 可搜索
        isCc: false,
        labelCol: { span: 4 },
        isCustomLabelCol: false
      },
      model: '', // 数据字段
      key: '',
      rules: [ // 校验规则
        {
          required: false,
          message: '必填项'
        }
      ]
    },
    {
      type: 'dept', // 组件类型
      label: '部门组件', // 组件名称
      labelHTML: '', // 标题增强
      icon: 'icon-zidingyiyemian',
      component: Dept, // 组件
      options: {
        defaultValue: '', // 默认值
        multiple: false, // 多选
        disabled: false, // 禁用
        hidden: false, // 是否隐藏，false显示，true隐藏
        width: '100%', // 宽度
        min: 0, // 最小值
        max: 99, // 最大值
        clearable: true, // 可清除
        placeholder: '请选择', // 占位内容
        showSearch: false, // 可搜索
        labelCol: { span: 4 },
        isCustomLabelCol: false
      },
      model: '', // 数据字段
      key: '',
      rules: [ // 校验规则
        {
          required: false,
          message: '必填项'
        }
      ]
    }
  ]
}
export default customConfig
